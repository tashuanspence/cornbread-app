<h3>Cornbread Soul App</h3>

Responsive Ionic/Angular pogressive web application for a popular growing soulfood chain in New Jersey, allowing customers to view the current menu and promotions. This was built with scalability in mind, deploying as a website with the ability to later be compiled as a native IOS and Android mobile application. This offered the flexibility to work with one code base, that when deployed any future updates can be pushed to all platforms at once, reducing the need for app resubmissions.
