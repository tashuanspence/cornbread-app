import { Component } from "@angular/core";
import {
  IonicPage,
  NavController,
  NavParams,
  ModalController,
  ViewController,
} from "ionic-angular";
import { DomSanitizer } from "@angular/platform-browser";
@IonicPage()
@Component({
  selector: "page-events",
  templateUrl: "events.html",
})
export class EventsPage {
  currEventObj: any;
  eventsArray = [];
  menuData = [
    {
      events: [
        {
          name: "Woman's History Month Jazz Night",
          date: "March 25, 2018",
          desc: "Woman's History Month Jazz Night",
          url: "url('http://cornbreadsoul.com/assets/event1.jpg')",
        },
      ],
    },
  ];
  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public modalCtrl: ModalController,
    private sanitizer: DomSanitizer
  ) {}
  createModal(event) {
    let profileModal = this.modalCtrl.create(EventProfile, {
      currEventObj: event,
    });
    profileModal.present();
  }
  ionViewDidLoad() {
    for (var i = 0; i < this.menuData[0].events.length; i++) {
      this.eventsArray.push(this.menuData[0].events[i]);
    }
  }
  sanitize(url): any {
    console.log(this.sanitizer.bypassSecurityTrustStyle(url));
    return this.sanitizer.bypassSecurityTrustStyle(url);
  }
}
@Component({
  selector: "event-profile",
  template:
    '<button id="closeBtn" (click)="closeClick()"><ion-icon name="close"></ion-icon></button><div id="eventImg" [style.backgroundImage]="sanitize(this.currEventObj.url)"></div><div id="eventInfo"><h2>{{this.currEventObj.title}}</h2><p id="desc">{{this.currEventObj.desc}}</p></div>',
})
export class EventProfile {
  currEventObj: any;
  constructor(
    params: NavParams,
    public viewCtrl: ViewController,
    private sanitizer: DomSanitizer
  ) {
    this.currEventObj = params.get("currEventObj");
  }
  closeClick() {
    this.viewCtrl.dismiss();
  }
  sanitize(url): any {
    //console.log('yo','url("' + this.sanitizer.bypassSecurityTrustStyle(url) + '")');
    return this.sanitizer.bypassSecurityTrustStyle(url);
  }
}
