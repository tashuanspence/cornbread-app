import { Component } from "@angular/core";
import {
  IonicPage,
  NavController,
  NavParams,
  ModalController,
} from "ionic-angular";
import { FoodProfile } from "../entrees/entrees";
import { DomSanitizer } from "@angular/platform-browser";
@IonicPage()
@Component({
  selector: "page-sides",
  templateUrl: "sides.html",
})
export class SidesPage {
  currFoodObj: any;
  sidesArray: any = [];
  menuData = [
    {
      sides: [
        {
          title: "Cornbread Candied Yam",
          picUrl: "url('http://cornbreadsoul.com/assets/img/yams.jpg')",
          portion:
            "One 4 ounce portion of roasted sweet potatoes and yams. butter, brown sugar, nuts.",
          desc: "A unique blend of garnet sweet potatoes and traditional yams, this dish is served in a butter and brown sugar base.",
          gluten:
            "without butter& Brown sugar = vegan.  Allergen/aversion = nuts",
          served: "Daily",
        },
        {
          title: "Collard Greens",
          picUrl: "url('http://cornbreadsoul.com/assets/img/salad3.jpg')",
          portion: "One 4 ounce portion of sautéed collards",
          desc: "Our collards are slow cooked until tender with garlic greens adding to their tasty appeal. Slow cooked until tender.",
          gluten: "Allergens/aversions=meat,brisket",
          served: "Daily",
        },
        {
          title: "Baked Mac n' Cheese",
          picUrl: "url('http://cornbreadsoul.com/assets/img/mac.jpg')",
          portion:
            "One 4 ounce portion of Macaroni and Cheese with Cornbread Crumbles",
          desc: "A decadent take on a Southern classic, cheddar cheese sauce and a special blend of shredded cheeses it is sure to please the pickiest palate.",
          gluten: "vegetarian",
          served: "Daily",
        },
        {
          title: "Rice",
          picUrl: "url('http://cornbreadsoul.com/assets/img/rice.jpg')",
          portion: "One 4 ounce portion of Brown Rice",
          desc: "Perfectly cooked fluffy rice, the perfect accompaniment to any dishes served with a sauce or gravy.",
          gluten: "Vegan",
          served: "Daily",
        },
        {
          title: "String Beans",
          picUrl: "url('http://cornbreadsoul.com/assets/img/string beans.jpg')",
          portion: "One 4 ounce portion of Southern style string beans",
          desc: "Fresh Green Beans cooked with our house smoked turkey stock make savory green beans that are a compliment for any entrée.",
          gluten: "allergen/aversion=meat,turkey",
          served: "Daily",
        },
        {
          title: "Cornbread Dressing",
          picUrl:
            "url('http://cornbreadsoul.com/assets/img/cornbread dressing.jpg')",
          portion: "One 4 ounce portion of southern style cornbread dressing",
          desc: "There is no other dish that puts the South in your mouth like Cornbread Dressing. Onions and celery sautéed in butter, along with vegetable broth with sage create the defining flavor profile for this traditional side dish.",
          gluten: "Allergens/Aversions=eggs, dairy",
          served: "Daily",
        },
      ],
      mixes: [],
    },
  ];
  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public modalCtrl: ModalController,
    public sanitizer: DomSanitizer
  ) {
    for (var i = 0; i < this.menuData[0].sides.length; i++) {
      this.sidesArray.push(this.menuData[0].sides[i]);
    }
  }
  createModal($event) {
    let thisTitle = $event.currentTarget.getAttribute("title");
    for (var i = 0; i < this.sidesArray.length; i++) {
      if (thisTitle == this.sidesArray[i].title) {
        this.currFoodObj = this.sidesArray[i];
      }
    }
    let profileModal = this.modalCtrl.create(FoodProfile, {
      currFoodObj: this.currFoodObj,
    });
    profileModal.present();
  }
  ionViewDidLoad() {
    console.log("ionViewDidLoad SidesPage");
  }
  sanitize(url): any {
    return this.sanitizer.bypassSecurityTrustStyle(url);
  }
}
