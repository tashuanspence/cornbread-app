import { Component } from "@angular/core";
import { IonicPage, NavController, NavParams } from "ionic-angular";
import { InAppBrowser } from "@ionic-native/in-app-browser";
@IonicPage()
@Component({
  selector: "page-order",
  templateUrl: "order.html",
})
export class OrderPage {
  constructor(
    private iab: InAppBrowser,
    public navCtrl: NavController,
    public navParams: NavParams
  ) {
    console.log("here");
  }
  ionViewDidLoad() {}

  gotoOnline() {
    this.iab.create("https://orders.cornbreadsoul.com");
  }
}
