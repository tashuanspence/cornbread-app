import { Component } from "@angular/core";
import {
  IonicPage,
  NavController,
  NavParams,
  Platform,
  ModalController,
  ViewController,
} from "ionic-angular";
import { Observable } from "rxjs/Observable";
import { CbServiceProvider } from "../../providers/cb-service/cb-service";
import { Camera, CameraOptions } from "@ionic-native/camera";
import { DomSanitizer } from "@angular/platform-browser";
@IonicPage()
@Component({
  selector: "page-photos",
  templateUrl: "photos.html",
})
export class PhotosPage {
  photos: Observable<any[]>;
  options: CameraOptions;
  selectedGal: any;
  photosArray: any = [];
  constructor(
    public modalCtrl: ModalController,
    private sanitizer: DomSanitizer,
    private cbServ: CbServiceProvider,
    private camera: Camera,
    public platform: Platform,
    public navCtrl: NavController,
    public navParams: NavParams
  ) {
    let date = new Date();
    let obj = {
      url: "http://via.placeholder.com/350x150",
      name: "test name",
      date: date.toDateString(),
    };
    // this.cbServ.addToGallery("/", "photos", obj, "photo").subscribe(data=>{
    //   console.log(data);
    // });
  }
  ionViewDidLoad() {
    this.cbServ.getGalleries();
  }
  galSelect() {
    this.cbServ
      .getImages("photos", "gallery", "==", this.selectedGal)
      .subscribe((docs) => {
        for (var i = 0; i < docs.length; i++) {
          this.photosArray.push(docs[i].data());
        }
      });
  }
  takePhoto() {
    this.options = {
      quality: 100,
      destinationType: this.camera.DestinationType.DATA_URL,
      encodingType: this.camera.EncodingType.JPEG,
      mediaType: this.camera.MediaType.PICTURE,
    };
    if (this.platform.is("cordova")) {
      this.camera.getPicture(this.options).then(
        (imageData) => {
          // save imageData to db with approval flag
          //write image url and data to DB
          let base64Image = "data:image/jpeg;base64," + imageData;
        },
        (err) => {
          // Handle error
        }
      );
    }
  }
  photoClick(photo) {
    let profileModal = this.modalCtrl.create(PhotoProfile, {
      currPhotoObj: photo,
    });
    profileModal.present();
  }
}
@Component({
  selector: "photo-profile",
  template:
    '<button id="closeBtn" (click)="closeClick()"><ion-icon name="close"></ion-icon></button><img id="photoImg" src="{{currPhotoObj.url}}"/><div id="photoInfo"><p id="desc">{{this.currPhotoObj.caption}}</p></div>',
})
export class PhotoProfile {
  currPhotoObj: any;
  constructor(
    params: NavParams,
    public viewCtrl: ViewController,
    private sanitizer: DomSanitizer
  ) {
    this.currPhotoObj = params.get("currPhotoObj");
  }
  closeClick() {
    this.viewCtrl.dismiss();
  }
  sanitize(url): any {
    console.log(this.sanitizer.bypassSecurityTrustStyle(url));
    return this.sanitizer.bypassSecurityTrustUrl(
      window.location.protocol + "//" + window.location.host + url
    );
  }
}
