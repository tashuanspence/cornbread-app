import { Component } from "@angular/core";
import { IonicPage, NavController, NavParams } from "ionic-angular";
import { InAppBrowser } from "@ionic-native/in-app-browser";
@IonicPage()
@Component({
  selector: "page-delivery",
  templateUrl: "delivery.html",
})
export class DeliveryPage {
  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    private iab: InAppBrowser
  ) {}

  ionViewDidLoad() {}

  openUber() {
    console.log("yooo");
    this.iab.create(
      "https://www.ubereats.com/new-jersey/food-delivery/cornbread/Y3uZaiqBQTq7iwAANgIwIg/"
    );
  }
}
