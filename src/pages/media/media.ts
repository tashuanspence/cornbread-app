import { Component } from "@angular/core";
import { IonicPage, NavController, NavParams } from "ionic-angular";
import { DomSanitizer } from "@angular/platform-browser";
@IonicPage()
@Component({
  selector: "page-media",
  templateUrl: "media.html",
})
export class MediaPage {
  mediaArray: any = [
    {
      thumb:
        "http://roinjlive.fyelxbzep.maxcdn-edge.com/wordpress/wp-content/uploads/2017/10/Corn-2.jpg",
      hdr: "Heart & soul: Cornbread, Bayoh’s new eatery, mixes business and pleasure",
      url: "http://www.roi-nj.com/2017/10/20/lifestyle/heart-soul-cornbread-bayohs-new-eatery-mixes-business-and-pleasure/",
    },
    {
      thumb:
        "https://villagegreennj.com/wp-content/uploads/2017/11/food-samples.jpg",
      hdr: "Let’s Bless This Place: Cornbread Restaurant Cuts Ribbon on Farm-to-Soul Family Affair",
      url: "https://villagegreennj.com/food-wellness/lets-bless-this-place-cornbread-restaurant-cuts-ribbon-on-farm-to-soul-family-affair/",
    },
    {
      thumb:
        "https://uploads.thealternativepress.com/uploads/photos/best_crop_2cb9cc0b295a87c0e984_0D474F35-9599-45A0-B3D1-DD47E0B276CB@2x.jpeg",
      hdr: "New Maplewood Restaurant: Cornbread Holds Ribbon Cutting and Grand Opening",
      url: "https://www.tapinto.net/towns/soma/articles/new-maplewood-restaurant-cornbread-holds-ribbon",
    },
    {
      thumb:
        "http://www.blackenterprise.com/wp-content/blogs.dir/1/files/2018/02/Adenah_Bayoh-1080x675.jpg",
      hdr: "THE MOST SUCCESSFUL LIBERIAN ENTREPRENEUR YOU (PROBABLY) NEVER HEARD OF",
      url: "http://www.blackenterprise.com/successful-liberian-entrepreneur-probably-never-heard/",
    },
  ];
  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    private sanitizer: DomSanitizer
  ) {}
  ionViewDidLoad() {}
  sanitize(url): any {
    console.log(url);
    return "url(" + this.sanitizer.bypassSecurityTrustStyle(url) + ")";
  }
  mediaClick(event) {
    window.open(this.mediaArray[event.currentTarget.id].url, "_blank");
  }
}
