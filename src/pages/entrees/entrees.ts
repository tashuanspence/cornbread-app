import { Component } from "@angular/core";
import {
  IonicPage,
  NavController,
  NavParams,
  ModalController,
  ViewController,
} from "ionic-angular";
import { DomSanitizer } from "@angular/platform-browser";
@IonicPage()
@Component({
  selector: "page-entrees",
  templateUrl: "entrees.html",
})
export class EntreesPage {
  currFoodObj: any;
  entreesArray = [];
  menuData = [
    {
      entrees: [
        {
          title: "Catfish Fried (2) - $9.99 ",
          picUrl: "url('http://cornbreadsoul.com/assets/img/catfish.jpg')",
          portion: "One 6oz-7oz portion of Catfish Fried in Cornmeal",
          desc: "Hand breaded and perfectly fried, our Mississippi catfish is crisp and crunchy southern delicacy.",
          gluten: "",
          served: "Daily",
        },
        {
          title: "Whiting Fried (3) - $9.99",
          picUrl: "url('http://cornbreadsoul.com/assets/img/whiting.jpg')",
          portion:
            "One 6oz-7oz portion of Whiting Fried in cornmeal served with house made tartar sauce",
          desc: "Boneless whiting dipped in a seasoned cornmeal batter, fried until crispy on the outside and fork tender on the inside.",
          gluten: "Gluten free / dairy free",
          served: "Daily",
        },
        //				{
        //				"title": "BBQ Chicken (3)",
        //				"picUrl": "url('http://cornbreadsoul.com/assets/img/bbq chicken.jpg')",
        //				"portion": "1/4 portion of chicken, dark & white meat",
        //				"desc": "Our mixed dark and white meat chicken slow smoked and basted in our house bbq sauce.",
        //				"gluten": "Gluten free / dairy free",
        //				"served": "Daily"
        //				},
        {
          title: "Fried Chicken (4) - $11.99",
          picUrl:
            "url('http://cornbreadsoul.com/assets/img/fried chicken.jpg')",
          portion: "1/4 portion of chicken, dark &  white meat",
          desc: "A generous portion of our mixed dark and white meat chicken fried golden brown.",
          gluten: "Gluten free / dairy free",
          served: "Daily",
        },
        {
          title: "Baked Chicken (4) - $11.99",
          picUrl:
            "url('http://cornbreadsoul.com/assets/img/baked chicken.jpg')",
          portion: "1/4 portion of chicken, dark & white meat",
          desc: "Our classic baked chicken recipe will leave you wanting more, golden brown on the outside, tender and juicy on the inside.  Served with a side of chicken gravy.",
          gluten:
            "Chicken is Gluten free/dairy free. Chicken Gravy contains dairy.",
          served: "Daily",
        },
        //				{
        //				"title": "Pork Chop (2) - $12.99",
        //				"picUrl": "url('http://cornbreadsoul.com/assets/img/pork chop.jpg')",
        //				"portion": "One 5.5 ounce portion of Brisket served with a side of BBQ sauce",
        //				"desc": "After 14 hours in the smoker, our house brisket is moist, rich, and tender. The delicate smoked flavor is complimented by our house made spice rub. Served with a side of our BBQ sauce.",
        //				"gluten": "Gluten free",
        //				"served": "Daily"
        //				},
        //				{
        //				"title": "Brisket (8oz) - $12.99",
        //				"picUrl": "url('http://cornbreadsoul.com/assets/img/brisket.jpg')",
        //				"portion": "One 6oz-7oz portion of Catfish Fried in Cornmeal",
        //				"desc": "Hand breaded and perfectly fried, our Mississippi catfish is crisp and crunchy southern delicacy.",
        //				"gluten": "Gluten free",
        //				"served": "Daily"
        //				},
        {
          title: "Smoked Turkey Wings (3) - $9.99",
          picUrl: "url('http://cornbreadsoul.com/assets/img/turkey.jpg')",
          portion: "Turkey Wings 3.5-4 ounce total weight",
          desc: "Marinated for 24 hours, our smoked turkey wings are cooked at a low temperature to impart major flavor while retaining moisture.",
          gluten: "Gluten free",
          served: "Daily",
        },
        //				{
        //				"title": "Smoked Sausage",
        //				"picUrl": "url('http://cornbreadsoul.com/assets/img/catfishDinner.jpg')",
        //				"portion": "",
        //				"desc": "We use aromatic wood and traditional recipes to give our smoked sausage a distinct flavor. No Nitrates ever.",
        //				"gluten": "Gluten free",
        //				"served": "Saturday and Sunday only"
        //				},
        //				{
        //				"title": "Bacon",
        //				"picUrl": "url('http://cornbreadsoul.com/assets/img/bacon.jpg')",
        //				"portion": "",
        //				"desc": "Hand cut, thick sliced bacon. A thick, savory flavor that adds just the right balance to any meal.",
        //				"gluten": "Gluten free",
        //				"served": "Saturday and Sunday only"
        //				},
        {
          title: "Oxtails (6) - $17.99",
          picUrl: "url('http://cornbreadsoul.com/assets/img/oxtails.jpg')",
          portion: "",
          desc: "Southern style oxtails smothered in a homemade onion and garlic gravy, and braised until they are fall off the bone tender.",
          gluten: "",
          served: "Saturday and Sunday only",
        },
        {
          title: "Smoked Ribs (6) - $17.99",
          picUrl: "url('http://cornbreadsoul.com/assets/img/ribs.jpg')",
          portion: "",
          desc: "Our house smoked ribs are basted in our own bbq sauce long enough that the meat literally slides off of the bone with the first bite. Tender and delicate, these are a house favorite.",
          gluten: "Gluten free",
          served: "Friday, Saturday and Sunday only",
        },
      ],
      mixes: [],
    },
  ];
  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public modalCtrl: ModalController,
    private sanitizer: DomSanitizer
  ) {}
  createModal($event) {
    let thisTitle = $event.currentTarget.getAttribute("title");
    for (var i = 0; i < this.entreesArray.length; i++) {
      if (thisTitle == this.entreesArray[i].title) {
        this.currFoodObj = this.entreesArray[i];
      }
    }
    let profileModal = this.modalCtrl.create(FoodProfile, {
      currFoodObj: this.currFoodObj,
    });
    profileModal.present();
  }
  ionViewDidLoad() {
    for (var i = 0; i < this.menuData[0].entrees.length; i++) {
      this.entreesArray.push(this.menuData[0].entrees[i]);
    }
  }
  sanitize(url): any {
    console.log(this.sanitizer.bypassSecurityTrustStyle(url));
    return this.sanitizer.bypassSecurityTrustStyle(url);
  }
}
@Component({
  selector: "food-profile",
  template:
    '<button id="closeBtn" (click)="closeClick()"><ion-icon name="close"></ion-icon></button><div id="foodImg" [style.backgroundImage]="sanitize(this.currFoodObj.picUrl)"></div><div id="foodInfo"><h2>{{this.currFoodObj.title}}</h2><p id="desc">{{this.currFoodObj.desc}}</p><p id="portion">{{this.currFoodObj.portion}}</p><p id="gluten">{{this.currFoodObj.gluten}}</p><p id="served">Served: {{this.currFoodObj.served}}</p></div>',
})
export class FoodProfile {
  currFoodObj: any;
  constructor(
    params: NavParams,
    public viewCtrl: ViewController,
    private sanitizer: DomSanitizer
  ) {
    this.currFoodObj = params.get("currFoodObj");
  }
  closeClick() {
    this.viewCtrl.dismiss();
  }
  sanitize(url): any {
    return this.sanitizer.bypassSecurityTrustStyle(url);
  }
}
