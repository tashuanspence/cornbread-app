import { Component } from "@angular/core";
import { IonicPage, NavController, NavParams } from "ionic-angular";
import { CbServiceProvider } from "../../providers/cb-service/cb-service";
@IonicPage()
@Component({
  selector: "page-cms",
  templateUrl: "cms.html",
})
export class CmsPage {
  newGalleryInput: any;
  galleries: any;
  caption: any;
  photoFile: any;
  selectedGal: any;
  constructor(
    public cbServ: CbServiceProvider,
    public navCtrl: NavController,
    public navParams: NavParams
  ) {
    this.getGalleries();
  }
  getGalleries() {
    this.cbServ.getGalleries();
  }
  newGallery() {
    if (this.newGalleryInput) {
      let obj = { name: this.newGalleryInput };
      this.cbServ.addGallery(obj).subscribe((data) => console.log(data));
    } else {
      alert("You must enter a gallery name.");
    }
  }
  addPhoto() {
    if (this.selectedGal && this.photoFile) {
      if (!this.caption) {
        this.caption = "none";
      }
      this.cbServ.uploadImage(
        this.selectedGal,
        this.photoFile,
        this.caption,
        this.photoFile
      );
    } else alert("You must select a photo gallery and image file.");
  }
  getPhoto(event) {
    this.photoFile = event.target.files;
  }
}
