import { Component, ViewChild, OnInit } from "@angular/core";
import { NavController } from "ionic-angular";
import { Slides } from "ionic-angular";
@Component({
  selector: "page-home",
  templateUrl: "home.html",
})
export class HomePage implements OnInit {
  @ViewChild(Slides) slides: Slides;
  constructor(public navCtrl: NavController) {}
  ngOnInit() {
    this.slides.startAutoplay();
  }

  goFb() {
    window.open("http://facebook.com/Cornbread-Maplewood", "_blank");
  }

  goIg() {
    window.open("http://instagram.com/cornbreadmaplewood", "_blank");
  }
}
