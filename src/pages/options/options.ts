import { Component } from "@angular/core";
import { IonicPage, NavController, NavParams } from "ionic-angular";
// import { MenuService } from '../services/menuService.ts';
/**
 * Generated class for the OptionsPage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */
@IonicPage()
@Component({
  selector: "page-options",
  templateUrl: "options.html",
})
export class OptionsPage {
  optionsArray = [];
  menuData = [
    {
      options: [
        {
          type: "Small Combo Meal",
          portion:
            "Meat plus one side with 1-piece Cornbread (see prices below)",
          footnote:
            "Turkey Wing, Fried Shrimp, Whiting or Catfish $12.99 Fried or Baked Chicken $14.99",
          price: "$11.99",
          served: "Daily",
        },
        {
          type: "Large Combo Meal",
          portion: "Single Portion Entrée, Two Sides, Cornbread.",
          footnote: "Option to order without cornbread, price is the same.",
          price: "$13.99",
          served: "Daily",
        },
        {
          type: "Family Meal",
          portion: "Entrée for 4, Two 8 ounce sides, cornbread for 4.",
          footnote:
            "Chicken Only. Option to order without cornbread, price is the same.",
          price: "$17.99",
          served: "Daily",
        },
        {
          type: "4 Vegetable Plate",
          portion: "4 portions of any 4 ounce side served with cornbread.",
          footnote: "Option to order without cornbread, price is the same.",
          price: "$9.99",
          served: "Daily",
        },
        {
          type: "House Salad",
          portion:
            "Mixed greens, cucumbers, tomatoes, smoked onions, sweet peppers, carrots, roasted corn,  cornbread croutons & pickled egg.",
          footnote:
            " Choice house vinaigrette on the side or herbed buttermilk ranch on the side.  Option of adding  pulled baked or bbq chicken for an additional fee. Can add extra chicken for an additional fee.",
          price: "$10.99",
          served: "Daily",
        },
        //			{
        //				"type": "Soup & Cornbread",
        //				"portion": "Chicken & SummerVegetable soup served with Cornbread. Can order without chicken and without cornbread.",
        //				"footnote": "Can order without chicken and without cornbread. Soup without chicken is vegan. Cornbread is vegetarian.",
        //				"price": "$11.99",
        //				"served": "Daily"
        //			}
        //			,
        {
          type: "Entrée Only",
          portion: "Single Portion any entrée.",
          footnote: "Price varies depending on entrée.",
          price: "",
          served: "Daily",
        },
        {
          type: "Cornbread Platter",
          portion:
            "Eggs any style: Scrambled with or without cheese. Served with Cornbread OR biscuits.  Served with Bacon or Sausage. Served with Grits or home fries.",
          footnote: "",
          price: "$12.99",
          served: "Saturday, Sunday",
        },
        {
          type: "Cornbread Benedict",
          portion:
            "2 scrambled eggs served over open face cornbread of biscuit with smoked sausage, hollendaise sauce and Sauteed green beans.",
          footnote: "Can subsitute any other side for green beans.",
          price: "$8.99",
          served: "Saturday, Sunday",
        },
        {
          type: "Hot Cakes",
          portion:
            "Served with butter and syrup, choice of bacon, sausage or any entrée",
          footnote: "",
          price: "$8.99",
          served: "Saturday, Sunday",
        },
      ],
      mixes: [],
    },
  ];
  constructor(public navCtrl: NavController, public navParams: NavParams) {}
  ionViewDidLoad() {
    for (var i = 0; i < this.menuData[0].options.length; i++) {
      this.optionsArray.push(this.menuData[0].options[i]);
    }
  }
}
