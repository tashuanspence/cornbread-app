import { Component } from "@angular/core";
import { IonicPage, NavController, NavParams, Platform } from "ionic-angular";
import { BarcodeScanner } from "@ionic-native/barcode-scanner";
@IonicPage()
@Component({
  selector: "page-barcode",
  templateUrl: "barcode.html",
})
export class BarcodePage {
  barcodeData: any = {};
  storage: any;
  isData: boolean = false;
  blinkStyle = ["off"];
  timer: any;
  constructor(
    private platform: Platform,
    private barcodeScanner: BarcodeScanner,
    public navCtrl: NavController,
    public navParams: NavParams
  ) {
    this.barcodeData.text = "";
  }
  ionViewDidLoad() {
    this.storage = window.localStorage;
    this.writeStorage();
    this.checkStorage();
    this.timer = setInterval(() => {
      if (this.blinkStyle[0] == "off") {
        this.blinkStyle = ["on"];
      } else {
        this.blinkStyle = ["off"];
      }
    }, 800);
  }
  ionViewWillLeave() {
    clearInterval(this.timer);
  }
  checkStorage() {
    if (this.storage.cbapp) {
      console.log(this.storage.cbapp);
      let txt = this.storage.cbapp;
      this.barcodeData = txt.split("_");
      console.log(this.barcodeData);
    } else {
      this.barcodeData = "";
    }
  }
  writeStorage() {
    this.storage.cbapp = "Save 20% off your next order over $20.00_001_3-12-18";
  }
  scan() {
    if (this.platform.is("cordova")) {
      this.barcodeScanner.scan().then((barcodeData) => {
        let txt = this.storage.cbapp;
        this.barcodeData = txt.split("_");
        if (!this.barcodeData[1] || !this.barcodeData[2]) {
          alert("You have scanned an invalid QR Code.");
        }
        if (this.storage.cbused) {
          let array = JSON.parse(this.storage.cbused);
          let isUsed = false;
          for (let i = 0; i < array.length; i++) {
            if (array[i] == this.barcodeData[1]) {
              alert("You have already used this coupon");
              isUsed = true;
              break;
            }
          }
          if (!isUsed) this.storage.cbapp = barcodeData.text;
        } else {
          this.storage.cbapp = barcodeData.text;
        }
      });
    }
  }
  redeem() {
    this.storage.removeItem("cbapp");
    if (!this.storage.cbused) this.storage.cbused = [];
    if (this.storage.cbused) {
      let array: Array<any> = JSON.parse(this.storage.cbused);
      array.push(this.barcodeData[1]);
      console.log(array);
      this.storage.cbused = JSON.stringify(array);
    } else {
      console.log("message");
      let array: Array<any> = [];
      array.push(this.barcodeData[1]);
      this.storage.cbused = JSON.stringify(array);
    }
    //this.storage.cbused = array;
    this.barcodeData = "";
  }
}
