import { NgModule } from "@angular/core";
import { IonicPageModule } from "ionic-angular";
import { BarcodePage } from "./barcode";
import { BarcodeScanner } from "@ionic-native/barcode-scanner";
@NgModule({
  declarations: [BarcodePage, BarcodeScanner],
  imports: [BarcodeScanner, IonicPageModule.forChild(BarcodePage)],
  entryComponents: [BarcodeScanner],
  providers: [BarcodeScanner],
})
export class BarcodePageModule {}
