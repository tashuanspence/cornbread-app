import { Component, ViewChild } from "@angular/core";
import { Nav, Platform } from "ionic-angular";
import { StatusBar } from "@ionic-native/status-bar";
import { SplashScreen } from "@ionic-native/splash-screen";
import { OptionsPage } from "../pages/options/options";
import { EntreesPage } from "../pages/entrees/entrees";
import { HomePage } from "../pages/home/home";
import { SidesPage } from "../pages/sides/sides";
import { AboutusPage } from "../pages/aboutus/aboutus";
import { DrinksPage } from "../pages/drinks/drinks";
import { MediaPage } from "../pages/media/media";
import { DeliveryPage } from "../pages/delivery/delivery";
import { OrderPage } from "../pages/order/order";
import { FCM } from "@ionic-native/fcm";
import { ToastController } from "ionic-angular";
import { BarcodePage } from "../pages/barcode/barcode";
import { EventsPage } from "../pages/events/events";
import { PhotosPage } from "../pages/photos/photos";
import { CmsPage } from "../pages/cms/cms";
@Component({
  templateUrl: "app.html",
})
export class MyApp {
  @ViewChild(Nav) nav: Nav;
  rootPage: any = HomePage;
  isCordova: boolean = false;
  pages: Array<{ title: string; component: any }>;
  constructor(
    private fcm: FCM,
    public platform: Platform,
    public statusBar: StatusBar,
    public splashScreen: SplashScreen,
    private toastCtrl: ToastController
  ) {
    this.initializeApp();
    // used for an example of ngFor and navigation
    this.pages = [
      { title: "Home", component: HomePage },
      { title: "Combos", component: OptionsPage },
      { title: "Entrees", component: EntreesPage },
      { title: "Sides", component: SidesPage },
      { title: "Dessert / Drinks", component: DrinksPage },
      { title: "About", component: AboutusPage },
      { title: "Media", component: MediaPage },
      { title: "Order Online", component: OrderPage },
      { title: "Delivery", component: DeliveryPage },
      { title: "Events", component: EventsPage },
      // { title: 'Photos', component: PhotosPage },
      { title: "Menu PDF", component: EventsPage },
    ];
  }
  initializeApp() {
    this.platform.ready().then(() => {
      if (this.platform.is("cordova")) {
        this.isCordova = true;
        this.pages.push({ title: "Scan Code", component: BarcodePage });
        this.splashScreen.hide();
        this.fcm.subscribeToTopic("all");
        this.fcm.onNotification().subscribe((data) => {
          if (data.wasTapped) {
          } else {
            this.presentToast(data.body);
          }
        });
      }
    });
  }
  openPage(page) {
    // Reset the content nav to have just this page
    // we wouldn't want the back button to show in this scenario
    console.log(page);
    if (page.title == "Menu PDF") {
      window.open(
        "http://cornbreadsoul.com/assets/img/Cornbread.pdf",
        "_blank"
      );
    } else {
      this.nav.setRoot(page.component);
    }
  }
  presentToast(msg) {
    let toast = this.toastCtrl.create({
      message: msg,
      duration: 3000,
      position: "top",
    });
    toast.onDidDismiss(() => {
      //console.log('Dismissed toast');
    });
    toast.present();
  }
}
