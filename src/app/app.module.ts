import { CbServiceProvider } from "../providers/cb-service/cb-service";
import { BrowserModule } from "@angular/platform-browser";
import { ErrorHandler, NgModule } from "@angular/core";
import { IonicApp, IonicErrorHandler, IonicModule } from "ionic-angular";
import { InAppBrowser } from "@ionic-native/in-app-browser";
import { MyApp } from "./app.component";
import { HomePage } from "../pages/home/home";
import { OptionsPage } from "../pages/options/options";
import { EntreesPage } from "../pages/entrees/entrees";
import { FoodProfile } from "../pages/entrees/entrees";
import { PhotoProfile } from "../pages/photos/photos";
import { EventProfile } from "../pages/events/events";
import { AboutusPage } from "../pages/aboutus/aboutus";
import { DrinksPage } from "../pages/drinks/drinks";
import { MediaPage } from "../pages/media/media";
import { CmsPage } from "../pages/cms/cms";
import { OrderPage } from "../pages/order/order";
import { EventsPage } from "../pages/events/events";
import { PhotosPage } from "../pages/photos/photos";
import { DeliveryPage } from "../pages/delivery/delivery";
import { SidesPage } from "../pages/sides/sides";
import { MissionComponent } from "../components/mission/mission";
import { CbHeaderComponent } from "../components/cb-header/cb-header";
import { CbFooterComponent } from "../components/cb-footer/cb-footer";
import { StatusBar } from "@ionic-native/status-bar";
import { SplashScreen } from "@ionic-native/splash-screen";
import { FCM } from "@ionic-native/fcm";
import { BarcodePage } from "../pages/barcode/barcode";
import { BarcodeScanner } from "@ionic-native/barcode-scanner";
import { AngularFireModule } from "angularfire2";
import { AngularFirestoreModule } from "angularfire2/firestore";
import { AngularFireStorageModule } from "angularfire2/storage";
import { Camera, CameraOptions } from "@ionic-native/camera";
import { HttpClientModule } from "@angular/common/http";
var config = {
  
};
@NgModule({
  declarations: [
    MyApp,
    HomePage,
    MissionComponent,
    OptionsPage,
    CmsPage,
    EntreesPage,
    SidesPage,
    MediaPage,
    DrinksPage,
    AboutusPage,
    CbHeaderComponent,
    FoodProfile,
    EventProfile,
    CbFooterComponent,
    DeliveryPage,
    OrderPage,
    BarcodePage,
    EventsPage,
    PhotosPage,
    PhotoProfile,
  ],
  imports: [
    HttpClientModule,
    BrowserModule,
    AngularFireModule.initializeApp(config),
    AngularFirestoreModule, // imports firebase/firestore, only needed for database features
    AngularFireStorageModule, // imports firebase/storage only needed for storage features
    IonicModule.forRoot(MyApp),
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    HomePage,
    CmsPage,
    MissionComponent,
    OptionsPage,
    EntreesPage,
    SidesPage,
    MediaPage,
    DrinksPage,
    AboutusPage,
    CbHeaderComponent,
    FoodProfile,
    EventProfile,
    CbFooterComponent,
    DeliveryPage,
    OrderPage,
    BarcodePage,
    EventsPage,
    PhotosPage,
    PhotoProfile,
  ],
  providers: [
    CbServiceProvider,
    StatusBar,
    SplashScreen,
    InAppBrowser,
    FCM,
    BarcodeScanner,
    Camera,
    HttpClientModule,
    { provide: ErrorHandler, useClass: IonicErrorHandler },
  ],
})
export class AppModule {}
