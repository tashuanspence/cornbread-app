import { Component } from "@angular/core";
import { InAppBrowser } from "@ionic-native/in-app-browser";
@Component({
  selector: "cb-footer",
  templateUrl: "cb-footer.html",
})
export class CbFooterComponent {
  text: string;
  constructor(private iab: InAppBrowser) {}

  goFb() {
    this.iab.create("https://www.facebook.com/CornbreadMaplewood/");
  }
  goIg() {
    this.iab.create("https://www.instagram.com/cornbreadmaplewood/?hl=en");
  }

  email() {
    window.open("mailto:contactus@cornbreadsoul.com", "_blank");
  }
}
