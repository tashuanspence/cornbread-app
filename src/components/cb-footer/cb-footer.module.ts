import { NgModule } from "@angular/core";
import { IonicPageModule } from "ionic-angular";
import { CbFooterComponent } from "./cb-footer";
@NgModule({
  declarations: [CbFooterComponent],
  imports: [IonicPageModule.forChild(CbFooterComponent)],
  exports: [CbFooterComponent],
})
export class CbFooterComponentModule {}
