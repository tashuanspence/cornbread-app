import { NgModule } from "@angular/core";
import { IonicPageModule } from "ionic-angular";
import { MissionComponent } from "./mission";
@NgModule({
  declarations: [MissionComponent],
  imports: [IonicPageModule.forChild(MissionComponent)],
  exports: [MissionComponent],
})
export class MissionComponentModule {}
