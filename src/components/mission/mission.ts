import { Component } from "@angular/core";
/**
 * Generated class for the MissionComponent component.
 *
 * See https://angular.io/docs/ts/latest/api/core/index/ComponentMetadata-class.html
 * for more info on Angular Components.
 */
@Component({
  selector: "mission",
  templateUrl: "mission.html",
})
export class MissionComponent {
  text: string;
  constructor() {
    console.log("Hello MissionComponent Component");
    this.text = "Hello World";
  }
}
