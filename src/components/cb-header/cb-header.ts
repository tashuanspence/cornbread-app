import { Component } from "@angular/core";
import { NavController } from "ionic-angular";
import { OptionsPage } from "../../pages/options/options";
import { EntreesPage } from "../../pages/entrees/entrees";
import { HomePage } from "../../pages/home/home";
import { SidesPage } from "../../pages/sides/sides";
import { AboutusPage } from "../../pages/aboutus/aboutus";
import { DrinksPage } from "../../pages/drinks/drinks";
import { MediaPage } from "../../pages/media/media";
import { DeliveryPage } from "../../pages/delivery/delivery";
import { OrderPage } from "../../pages/order/order";
import { EventsPage } from "../../pages/events/events";
import { PhotosPage } from "../../pages/photos/photos";
@Component({
  selector: "cb-header",
  templateUrl: "cb-header.html",
})
export class CbHeaderComponent {
  constructor(public navCtrl: NavController) {}
  menuClick($event) {
    let thisPage;
    switch ($event.currentTarget.id) {
      case "homeBtn":
        thisPage = HomePage;
        break;
      case "entreesBtn":
        thisPage = EntreesPage;
        break;
      case "sidesBtn":
        thisPage = SidesPage;
        break;
      case "combosBtn":
        thisPage = OptionsPage;
        break;
      case "aboutBtn":
        thisPage = AboutusPage;
        break;
      case "drinksBtn":
        thisPage = DrinksPage;
        break;
      case "mediaBtn":
        thisPage = MediaPage;
        break;
      case "deliveryBtn":
        thisPage = DeliveryPage;
        break;
      case "orderBtn":
        thisPage = OrderPage;
        break;
      case "eventsBtn":
        thisPage = EventsPage;
        break;
      case "photosBtn":
        thisPage = PhotosPage;
        break;
      case "pdfBtn":
        window.open(
          "http://cornbreadsoul.com/assets/img/Cornbread.pdf",
          "_blank"
        );
        break;
      default:
        thisPage = HomePage;
    }
    console.log(thisPage);
    this.navCtrl.setRoot(thisPage);
  }
}
