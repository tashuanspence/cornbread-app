import { NgModule } from "@angular/core";
import { IonicPageModule } from "ionic-angular";
import { CbHeaderComponent } from "./cb-header";
@NgModule({
  declarations: [CbHeaderComponent],
  imports: [IonicPageModule.forChild(CbHeaderComponent)],
  exports: [CbHeaderComponent],
})
export class CbHeaderComponentModule {}
