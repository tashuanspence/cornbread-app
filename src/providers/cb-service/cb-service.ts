import { HttpClient } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { AngularFirestore } from "angularfire2/firestore";
import { Observable } from "rxjs/Observable";
import * as firebase from "firebase/app";
import * as fbStorage from "firebase/storage";
@Injectable()
export class CbServiceProvider {
  photos: any = ["url test"];
  galleries: any = [];
  storageRef: any;
  constructor(public http: HttpClient, private db: AngularFirestore) {
    firebase;
    fbStorage;
    var thisStorage = firebase.storage();
    // Create a storage reference from our storage service
    this.storageRef = thisStorage.ref();
    this.db = firebase.firestore();
    //this.getImages("photos")
    //this.uploadImage();
  }
  addGallery(obj) {
    let docRef = this.db.doc("/");
    return new Observable((observer) => {
      docRef
        .collection("galleries")
        .doc(obj.name)
        .set(obj, { merge: true })
        .then((docRef) => {
          observer.next("complete");
          this.galleries.push(obj.name);
        });
    });
  }
  getImages(coll, key, op, val): Observable<any> {
    return new Observable((observer) => {
      this.db
        .collection(coll)
        .where(key, op, val)
        .get()
        .then(function (querySnapshot) {
          observer.next(querySnapshot.docs);
        })
        .catch(function (error) {
          console.log("Error getting documents: ", error);
        });
    });
  }
  getGalleries(): Observable {
    this.db
      .collection("galleries")
      .get()
      .then((querySnapshot) => {
        querySnapshot.forEach((doc) => {
          let data = doc.data();
          this.galleries.push(data.name);
        });
      });
  }
  uploadImage(gallery, photo, caption, file) {
    file = file[0];
    var uploadTask = this.storageRef.child("photos/" + file.name).put(file);
    let date = new Date();
    // Listen for state changes, errors, and completion of the upload.
    uploadTask.on(
      firebase.storage.TaskEvent.STATE_CHANGED, // or 'state_changed'
      (snapshot) => {
        // Get task progress, including the number of bytes uploaded and the total number of bytes to be uploaded
        var progress = (snapshot.bytesTransferred / snapshot.totalBytes) * 100;
        //console.log('Upload is ' + progress + '% done');
        switch (snapshot.state) {
          case firebase.storage.TaskState.PAUSED: // or 'paused'
            //console.log('Upload is paused');
            break;
          case firebase.storage.TaskState.RUNNING: // or 'running'
            //console.log('Upload is running');
            break;
        }
      },
      function (error) {},
      () => {
        // Upload completed successfully, now we can get the download URL
        let obj = {
          url: uploadTask.snapshot.downloadURL,
          gallery: gallery,
          caption: caption,
          date: date.toDateString(),
          id: Math.floor(Math.random() * 99999 + 1),
        };
        this.addPhoto(obj);
      }
    );
  }
  saveImg() {}
  removeImg() {}
  addPhoto(obj) {
    this.db.doc("photos/" + obj.id).set(obj);
    // let docRef = this.db.doc("/")
    //
    // let id = "photo_" + Math.floor((Math.random() * 99999) + 1);
    //
    //        return new Observable(observer=>{
    //            docRef.collection("photos").doc("photo").set(obj, {merge: true }).then((docRef)=> {
    //                 observer.next("complete");
    //                 //this.galleries.push(obj.name)
    //           })
    //       })
  }
  createGallery() {}
  removeGallery() {}
}
